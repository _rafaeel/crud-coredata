//
//  Patient.m
//  CoreDataApplication
//
//  Created by Rafael on 6/17/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <RestKit/RestKit.h>
#import "Patient.h"
#import "Prescription.h"
#import "AppDelegate.h"

@implementation Patient

@dynamic firstName;
@dynamic lastName;
@dynamic age;
@dynamic prescriptions;

+ (NSArray *)syncPatients
{
//    RKObjectManager *manager = [RKObjectManager sharedManager];
//    RKObjectMapping *objectMapping = [RKObjectMapping mappingForClass:[self class]];
//    [objectMapping addAttributeMappingsFromArray:@[@"firstName", @"lastName", @"age"]];
//    
//    NSIndexSet *statusCode = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
//    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:objectMapping
//                                                                                            method:RKRequestMethodAny
//                                                                                       pathPattern:@"/patients.json"
//                                                                                           keyPath:@"patients"
//                                                                                       statusCodes:statusCode];
    
    NSString *getPath = @"/patients.json";
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    NSError *error;
    BOOL success = RKEnsureDirectoryExistsAtPath(RKApplicationDataDirectory(), &error);
    if (!success) RKLogError(@"Failed to create Application Data Directory at path '%@': %@", RKApplicationDataDirectory(), error);
    
    NSString *path = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"CoreDataApplication.sqlite"];
    
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:path
                                                                     fromSeedDatabaseAtPath:nil
                                                                          withConfiguration:nil
                                                                                    options:@{
                                                                                              NSInferMappingModelAutomaticallyOption: @YES,
                                                                                              NSMigratePersistentStoresAutomaticallyOption: @YES
                                                                                              }
                                                                                      error:&error];
    if (!persistentStore) RKLogError(@"Failed adding persistent store at path '%@': %@", path, error);
    [managedObjectStore createManagedObjectContexts];
    
    RKEntityMapping *patientMap = [RKEntityMapping mappingForEntityForName:NSStringFromClass([self class]) inManagedObjectStore:managedObjectStore];
    [patientMap addAttributeMappingsFromArray:@[@"firstName", @"lastName", @"age"]];
    NSIndexSet *statusCode = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:patientMap
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:getPath
                                                                                           keyPath:@"patients"
                                                                                       statusCodes:statusCode];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[[AppDelegate baseURL] stringByAppendingString:getPath]]];
    RKManagedObjectRequestOperation *operation = [[RKManagedObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setManagedObjectCache:[managedObjectStore managedObjectCache]];
    [operation setManagedObjectContext:[managedObjectStore mainQueueManagedObjectContext]];
    
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSLog(@"Response: %@", operation.HTTPRequestOperation.responseString);
        
//        Patient *p = [result firstObject];
        NSLog(@"Mapped the patient: %@", result.description);
        
        NSDictionary *sendInfo = [NSDictionary dictionaryWithObject:[result array] forKey:@"patientsSent"];
        
       [[NSNotificationCenter defaultCenter] postNotificationName:@"kSyncAction" object:nil userInfo:sendInfo];
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"Failed with error: %@", [error localizedDescription]);
    }];
    NSOperationQueue *operationQueue = [NSOperationQueue new];
    [operationQueue addOperation:operation];
    
    /*
    [manager addResponseDescriptor:responseDescriptor];
    [manager getObjectsAtPath:@"/patients.json" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *result){
                          
        Patient *p = [[result array] firstObject];
        NSLog(@"firstname: %@", p.firstName);
        
    }
    failure:^(RKObjectRequestOperation *operation, NSError *error)
    {
        RKLogError(@"Error: %@", error);
        RKLogError(@"Operation failed with error: %@", error.localizedDescription);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }];
    */
    
    return [[NSArray alloc] init];
}

@end
