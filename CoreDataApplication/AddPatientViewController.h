//
//  AddPatientViewController.h
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreViewController.h"
#import "Patient.h"

@interface AddPatientViewController : CoreViewController

@property (strong, nonatomic) Patient *addNewPatient;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;

- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)save:(UIBarButtonItem *)sender;

@end
