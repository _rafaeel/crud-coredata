//
//  CoreViewController.m
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import "CoreViewController.h"

@interface CoreViewController ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation CoreViewController

- (NSManagedObjectContext *)managedObjectContext
{
    return [(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

- (void)cancelAndDismiss
{
    [self.managedObjectContext rollback];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveAndDismiss
{
    NSError *error;
    if ([self.managedObjectContext hasChanges]) {
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Error on Save: %@", [error localizedDescription]);
        } else {
            NSLog(@"Saved Succesfully!");
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
