//
//  PrescriptionsTableViewController.h
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Patient.h"
#import "Prescription.h"
#import "PrescriptionsTableViewController.h"
#import "AppDelegate.h"

@interface PrescriptionsTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) Patient *selectedPatient;

@end
