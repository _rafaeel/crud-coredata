//
//  AddPrescriptionViewController.h
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreViewController.h"
#import "Patient.h"
#import "Prescription.h"

@interface AddPrescriptionViewController : CoreViewController

@property (strong, nonatomic) Patient *prescriptionsPatient;
@property (strong, nonatomic) Prescription *addNewPrescription;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *instructionsTextField;

- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)save:(UIBarButtonItem *)sender;

@end
