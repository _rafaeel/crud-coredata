//
//  Prescription.m
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import "Prescription.h"


@implementation Prescription

@dynamic name;
@dynamic instructions;
@dynamic patient;

@end
