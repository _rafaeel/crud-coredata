//
//  EditPatientViewController.h
//  CoreDataApplication
//
//  Created by Rafael on 6/17/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreViewController.h"
#import "Patient.h"

@interface EditPatientViewController : CoreViewController

@property (strong, nonatomic) Patient *editPatient;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

- (IBAction)cancel:(UIBarButtonItem *)sender;
- (IBAction)edit:(UIBarButtonItem *)sender;

@end
