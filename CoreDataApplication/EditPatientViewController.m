//
//  EditPatientViewController.m
//  CoreDataApplication
//
//  Created by Rafael on 6/17/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import "EditPatientViewController.h"

@interface EditPatientViewController ()

@end

@implementation EditPatientViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.firstNameTextField setEnabled:NO];
    [self.lastNameTextField setEnabled:NO];
    [self.ageTextField setEnabled:NO];
    
    [self.firstNameTextField setBorderStyle:UITextBorderStyleNone];
    [self.lastNameTextField setBorderStyle:UITextBorderStyleNone];
    [self.ageTextField setBorderStyle:UITextBorderStyleNone];
    
    self.firstNameTextField.text = self.editPatient.firstName;
    self.lastNameTextField.text = self.editPatient.lastName;
    self.ageTextField.text = [NSString stringWithFormat:@"%@", self.editPatient.age];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(UIBarButtonItem *)sender
{
    [super cancelAndDismiss];
}

- (IBAction)edit:(UIBarButtonItem *)sender
{
    if ([self.editButton.title isEqualToString:@"Edit"]) {
        self.editButton.title = @"Done";
        self.navigationItem.title = @"Edit Patient";
        
        [self.firstNameTextField setEnabled:YES];
        [self.lastNameTextField setEnabled:YES];
        [self.ageTextField setEnabled:YES];
        
        [self.firstNameTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [self.lastNameTextField setBorderStyle:UITextBorderStyleRoundedRect];
        [self.ageTextField setBorderStyle:UITextBorderStyleRoundedRect];
    } else if ([self.editButton.title isEqualToString:@"Done"]) {
        self.editButton.title = @"Edit";
        self.navigationItem.title = @"Patient";
        
        [self.firstNameTextField setEnabled:NO];
        [self.lastNameTextField setEnabled:NO];
        [self.ageTextField setEnabled:NO];
        
        [self.firstNameTextField setBorderStyle:UITextBorderStyleNone];
        [self.lastNameTextField setBorderStyle:UITextBorderStyleNone];
        [self.ageTextField setBorderStyle:UITextBorderStyleNone];
        
        self.editPatient.firstName = self.firstNameTextField.text;
        self.editPatient.lastName = self.lastNameTextField.text;
        self.editPatient.age = [NSDecimalNumber decimalNumberWithString:self.ageTextField.text];
        
        [super saveAndDismiss];
    }
}
@end
