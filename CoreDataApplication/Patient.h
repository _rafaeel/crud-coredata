//
//  Patient.h
//  CoreDataApplication
//
//  Created by Rafael on 6/17/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Prescription;

@interface Patient : NSManagedObject

@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSDecimalNumber * age;
@property (nonatomic, retain) NSSet *prescriptions;

+ (NSArray *)syncPatients;

@end

@interface Patient (CoreDataGeneratedAccessors)

- (void)addPrescriptionsObject:(Prescription *)value;
- (void)removePrescriptionsObject:(Prescription *)value;
- (void)addPrescriptions:(NSSet *)values;
- (void)removePrescriptions:(NSSet *)values;

@end
