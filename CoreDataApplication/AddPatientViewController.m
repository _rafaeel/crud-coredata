//
//  AddPatientViewController.m
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import "AddPatientViewController.h"

@interface AddPatientViewController ()

@end

@implementation AddPatientViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(UIBarButtonItem *)sender
{
    [super cancelAndDismiss];
}

- (IBAction)save:(UIBarButtonItem *)sender
{
    self.addNewPatient.lastName = self.lastNameTextField.text;
    self.addNewPatient.firstName = self.firstNameTextField.text;
    self.addNewPatient.age = [NSDecimalNumber decimalNumberWithString:self.ageTextField.text];
    [super saveAndDismiss];
}
@end
