//
//  CoreViewController.h
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CoreViewController : UIViewController

- (void)cancelAndDismiss;
- (void)saveAndDismiss;

@end
