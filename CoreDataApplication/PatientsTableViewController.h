//
//  PatientsTableViewController.h
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Patient.h"

@interface PatientsTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) Patient *patient;

- (IBAction)sync:(id)sender;

@end
