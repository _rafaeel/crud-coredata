//
//  Prescription.h
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Prescription : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * instructions;
@property (nonatomic, retain) NSManagedObject *patient;

@end
