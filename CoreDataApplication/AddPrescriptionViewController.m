//
//  AddPrescriptionViewController.m
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import "AddPrescriptionViewController.h"
#import "PrescriptionsTableViewController.h"

@interface AddPrescriptionViewController ()

@end

@implementation AddPrescriptionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(UIBarButtonItem *)sender
{
    [super cancelAndDismiss];
}

- (IBAction)save:(UIBarButtonItem *)sender
{
    self.addNewPrescription.patient = self.prescriptionsPatient;
    self.addNewPrescription.name = self.nameTextField.text;
    self.addNewPrescription.instructions = self.instructionsTextField.text;
    [super saveAndDismiss];
}
@end
