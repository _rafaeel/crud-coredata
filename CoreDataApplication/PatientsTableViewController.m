//
//  PatientsTableViewController.m
//  CoreDataApplication
//
//  Created by Rafael on 6/16/14.
//  Copyright (c) 2014 Rafael Rocha. All rights reserved.
//

#import "PatientsTableViewController.h"
#import "AppDelegate.h"
#import "AddPatientViewController.h"
#import "EditPatientViewController.h"
#import "PrescriptionsTableViewController.h"

@interface PatientsTableViewController ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation PatientsTableViewController

- (NSManagedObjectContext *)managedObjectContext
{
    return[(AppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Error: %@", [error localizedDescription]);
        abort();
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    Patient *patient = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = patient.firstName;
    cell.detailTextLabel.text = patient.lastName;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        NSManagedObjectContext *context = [self managedObjectContext];
        Patient *deletePatient = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [context deleteObject:deletePatient];
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Error: %@", [error localizedDescription]);
        }
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AddPatient"]) {
        UINavigationController *nvc = [segue destinationViewController];
        AddPatientViewController *apvc = (AddPatientViewController *) nvc.topViewController;
        Patient *addPatient = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([Patient class])
                                                            inManagedObjectContext:[self managedObjectContext]];
        apvc.addNewPatient = addPatient;
    } else if ([segue.identifier isEqualToString:@"ListPrescriptions"]) {
        PrescriptionsTableViewController *ptvc = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Patient *selectedPatient = [self.fetchedResultsController objectAtIndexPath:indexPath];
        ptvc.selectedPatient = selectedPatient;
    } else if ([segue.identifier isEqualToString:@"EditPatient"]) {
        UINavigationController *nvc = [segue destinationViewController];
        EditPatientViewController *epvc = (EditPatientViewController *) nvc.topViewController;
        NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *) sender];
        Patient *editPatient = [self.fetchedResultsController objectAtIndexPath:indexPath];
        epvc.editPatient = editPatient;
    }
}

#pragma mark - Fetched Results Controller Section

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) return _fetchedResultsController;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([Patient class])
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    //Sort attributes
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    fetchRequest.sortDescriptors = sortDescriptors;
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                    managedObjectContext:context
                                                                      sectionNameKeyPath:nil
                                                                               cacheName:nil];
    _fetchedResultsController.delegate = self;
    return _fetchedResultsController;
}

#pragma mark - Fetched Results Controller Delegates

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *placeholderTV = self.tableView; //creating a temporary placeholder
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [placeholderTV insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [placeholderTV deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
        {
            Patient *editPatient = [self.fetchedResultsController objectAtIndexPath:indexPath];
            UITableViewCell *cell = [placeholderTV cellForRowAtIndexPath:indexPath];
            cell.textLabel.text = editPatient.firstName;
        }
            break;
            
        case NSFetchedResultsChangeMove:
        {
            [placeholderTV deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
            [placeholderTV insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                                 withRowAnimation:UITableViewRowAnimationFade];
        }
            
        default:
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller
  didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex
     forChangeType:(NSFetchedResultsChangeType)type
{
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            break;
    }
}

#pragma mark - Sync action

UIAlertView *loading;

- (IBAction)sync:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(syncDidFinish:) name:@"kSyncAction" object:nil];
    
    loading = [[UIAlertView alloc] initWithTitle:@"Loading"
                                                      message:nil
                                                     delegate:nil
                                            cancelButtonTitle:nil
                                            otherButtonTitles:nil, nil];
    [loading show];
    
    [Patient syncPatients];
}

#pragma mark - Sync delegate

- (void)syncDidFinish:(NSNotification *)notification
{
    NSError *error;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Error: %@", [error localizedDescription]);
        abort();
    }
    [self.tableView reloadData];
    
    [loading dismissWithClickedButtonIndex:-1 animated:YES];
}

@end
